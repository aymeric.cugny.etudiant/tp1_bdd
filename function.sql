create or replace function date_creation(integer) 
returns date as '
    select min(date_debut_etat_a) FROM histo_article WHERE id_article = $1;
' 
language 'sql';

create or replace function date_creation(histo_article) 
returns date as '
    select min(date_debut_etat_a) FROM histo_article WHERE id_article = $1.id_article;
' 
language 'sql';

create or replace function nb_images(article_image)
returns bigint as '
    select count(*) from image where id_image = $1.id_image;
'
language 'sql'; 

create or replace function nb_art_avec_image(article_image)
returns bigint as '
    select count(*) from article where id_article = $1.id_article; 
'
language 'sql'; 

create or replace function nb_art_et_breves(rubrique) 
returns bigint 