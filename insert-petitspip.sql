-- create table journaliste(id_journaliste serial primary key, nom_journaliste varchar(20), prenom_journaliste varchar(20),
-- 		mel_journaliste varchar(100));

insert into journaliste(nom_journaliste, prenom_journaliste, mel_journaliste) values('Dupont','Jean','jean.dupont@monmail.fr');
insert into journaliste(nom_journaliste, prenom_journaliste, mel_journaliste) values('Blondin','Antoine','balthazar.dubois@monmail.fr');
insert into journaliste(nom_journaliste, prenom_journaliste, mel_journaliste) values('Durand','Paul','paul.durand@monmail.fr');



-- create table rubrique(id_rubrique serial primary key,
-- 		titre_rubrique varchar(20),
-- 		intro_rubrique varchar(250),
-- 		id_rubrique_sup int references rubrique on delete set null on update cascade,
-- 		id_image int references image on delete set null on update cascade);

insert into rubrique(titre_rubrique) values ('Quoi de neuf ?');
insert into rubrique(titre_rubrique) values ('Rubrique 1');
insert into rubrique(titre_rubrique) values ('Rubrique 2');
insert into rubrique(titre_rubrique) values ('super-rubrique');
update rubrique
set id_rubrique_sup = 4
where id_rubrique <> 4;

-- create table article(id_article serial primary key,
-- 		titre_article varchar(20),
-- 		texte_article text,
-- 		chapo_article varchar(250),
-- 		id_rubrique int references rubrique on delete restrict on update cascade);

insert into article(titre_article,id_rubrique) values ('titre1',2);
insert into article(titre_article,id_rubrique) values ('titre2',2);
insert into article(titre_article,id_rubrique) values ('titre3',2);
insert into article(titre_article,id_rubrique) values ('titre4',3);
insert into article(titre_article,id_rubrique) values ('titre5',3);
insert into article(titre_article,id_rubrique) values ('titre6',3);
insert into article(titre_article,id_rubrique) values ('titre7',3);
insert into article(titre_article,id_rubrique) values ('titre8',2);
insert into article(titre_article,id_rubrique) values ('titre9',1);
insert into article(titre_article,id_rubrique) values ('titre10',1);

-- create table breve(id_breve serial primary key,
-- 		titre_breve varchar(20),
-- 		texte_breve varchar(250),
-- 		id_rubrique int references rubrique on delete restrict on update cascade,
-- 		id_journaliste int references journaliste on delete set null on update cascade);

insert into breve(titre_breve,id_rubrique) values ('titre1b',3);
insert into breve(titre_breve,id_rubrique) values ('titre2b',1);
insert into breve(titre_breve,id_rubrique) values ('titre3b',1);
insert into breve(titre_breve,id_rubrique) values ('titre4b',1);
insert into breve(titre_breve,id_rubrique) values ('titre5b',3);
insert into breve(titre_breve,id_rubrique) values ('titre6b',1);
insert into breve(titre_breve,id_rubrique) values ('titre7b',3);
insert into breve(titre_breve,id_rubrique) values ('titre8b',3);

-- create table piece_jointe(id_piece_jointe serial primary key,
-- 		nom_piece_jointe varchar(20),
-- 		url_piece_jointe varchar(100));

insert into piece_jointe(id_piece_jointe) values (1);
insert into piece_jointe(id_piece_jointe) values (2);
insert into piece_jointe(id_piece_jointe) values (3);
insert into piece_jointe(id_piece_jointe) values (4);
insert into piece_jointe(id_piece_jointe) values (5);
insert into piece_jointe(id_piece_jointe) values (6);




-- create table article_piece_jointe(id_article int references article on delete cascade on update cascade,
-- 		id_piece_jointe int references piece_jointe on delete cascade on update cascade,
-- 		largeur_image int,
-- 		primary key(id_article, id_piece_jointe));

insert into article_piece_jointe(id_article, id_piece_jointe) values(1,1);
insert into article_piece_jointe(id_article, id_piece_jointe) values(1,2);
insert into article_piece_jointe(id_article, id_piece_jointe) values(1,3);
insert into article_piece_jointe(id_article, id_piece_jointe) values(4,4);
insert into article_piece_jointe(id_article, id_piece_jointe) values(4,5);
insert into article_piece_jointe(id_article, id_piece_jointe) values(4,6);
insert into article_piece_jointe(id_article, id_piece_jointe) values(2,1);
insert into article_piece_jointe(id_article, id_piece_jointe) values(2,2);
insert into article_piece_jointe(id_article, id_piece_jointe) values(3,1);
insert into article_piece_jointe(id_article, id_piece_jointe) values(3,4);


-- create table journaliste_article(id_journaliste int references journaliste on delete cascade on update cascade,
-- 		id_article int references article on delete cascade on update cascade,
-- 		primary key(id_journaliste, id_article));

insert into journaliste_article(id_journaliste,id_article) values (1,1);  -- rub 2
insert into journaliste_article(id_journaliste,id_article) values (1,2);  -- rub 2
insert into journaliste_article(id_journaliste,id_article) values (1,3);  -- rub 2
insert into journaliste_article(id_journaliste,id_article) values (2,4);  -- rub 3
insert into journaliste_article(id_journaliste,id_article) values (2,5);  -- rub 3
insert into journaliste_article(id_journaliste,id_article) values (2,6);  -- rub 3
insert into journaliste_article(id_journaliste,id_article) values (3,7);  -- rub 3
insert into journaliste_article(id_journaliste,id_article) values (3,8);  -- rub 2
insert into journaliste_article(id_journaliste,id_article) values (3,1);  -- rub 2
insert into journaliste_article(id_journaliste,id_article) values (2,2);  -- rub 2
insert into journaliste_article(id_journaliste,id_article) values (3,10);  -- rub 2
insert into journaliste_article(id_journaliste,id_article) values (2,9);  -- rub 2

-- create table statut(id_statut serial primary key, nom_statut varchar(20), descriptif_statut varchar(250));

insert into statut(nom_statut) values('responsable');
insert into statut(nom_statut) values('collaborateur');


-- create table journaliste_statut(id_journaliste int references journaliste on delete cascade on update cascade,
-- 		id_statut int references statut on delete cascade on update cascade,
-- 		id_rubrique int references rubrique on delete cascade on update cascade,
-- 		primary key(id_journaliste, id_statut, id_rubrique));

insert into journaliste_statut(id_journaliste,id_statut,id_rubrique) values(1,1,1);
insert into journaliste_statut(id_journaliste,id_statut,id_rubrique) values(2,1,2);
insert into journaliste_statut(id_journaliste,id_statut,id_rubrique) values(3,1,3);
insert into journaliste_statut(id_journaliste,id_statut,id_rubrique) values(1,2,2);
insert into journaliste_statut(id_journaliste,id_statut,id_rubrique) values(3,2,2);
insert into journaliste_statut(id_journaliste,id_statut,id_rubrique) values(2,2,1);
insert into journaliste_statut(id_journaliste,id_statut,id_rubrique) values(3,2,1);


-- create table etat(id_etat serial primary key, nom_etat varchar(20), descriptif_etat varchar(250));

insert into etat(nom_etat) values ('en ligne');
insert into etat(nom_etat) values ('en cours rédaction');
insert into etat(nom_etat) values ('à valider');
insert into etat(nom_etat) values ('off line');


-- create table histo_article(id_article int references article on delete cascade on update cascade,
-- 		id_etat int references etat on delete cascade on update cascade,
-- 		date_debut_etat_a date, date_fin_etat_a date,
-- 		primary key(id_article, id_etat, date_debut_etat_a));

insert into histo_article(id_article, id_etat, date_debut_etat_a,date_fin_etat_a) values (1,3,'2019-12-12','2020-06-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a,date_fin_etat_a) values (2,3,'2019-12-12','2020-01-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a,date_fin_etat_a) values (3,3,'2019-12-12','2020-02-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a,date_fin_etat_a) values (3,1,'2020-02-12','2021-02-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a,date_fin_etat_a) values (4,3,'2021-12-12','2020-05-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (1,1,'2020-08-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (2,3,'2020-08-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (3,4,'2021-02-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (4,1,'2020-07-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (5,1,'2020-06-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (6,1,'2020-05-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (7,2,'2020-09-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (8,2,'2020-07-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (9,2,'2020-09-12');
insert into histo_article(id_article, id_etat, date_debut_etat_a) values (10,2,'2020-07-12');


-- create table histo_breve(id_breve int references breve on delete cascade on update cascade,
-- 		id_etat int references etat on delete cascade on update cascade,
-- 		date_debut_etat_b date, date_fin_etat_b date,
-- 		primary key(id_breve, id_etat, date_debut_etat_b));

insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(1,1,'2021-08-03');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(2,2,'2021-02-05');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(3,3,'2021-08-16');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(4,4,'2021-03-07');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b,date_fin_etat_b) values(4,1,'2020-03-02','2021-03-07');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(5,1,'2021-08-01');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(6,2,'2020-08-12');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(7,3,'2020-08-02');
insert into histo_breve(id_breve, id_etat, date_debut_etat_b) values(8,4,'2020-08-03');


-- create table image(id_image serial primary key,
-- 		legende_image varchar(20),
-- 		url_image varchar(100));

insert into image(legende_image, url_image) values('ma photo', 'http://uneadresse/maphoto.png');
insert into image(legende_image, url_image) values('un chat', 'http://uneadresse/cat.png');
insert into image(legende_image, url_image) values('un chien', 'http://uneadresse/dog.png');
insert into image(legende_image, url_image) values('un poney', 'http://uneadresse/poney.png');


-- create table article_image(
--     id_article int references article on delete cascade on update cascade,
--     id_image int references image on delete cascade on update cascade,
--     largeur_image int,
--     primary key(id_article, id_image));

insert into article_image(id_article, id_image, largeur_image) values(7, 1, 150);
insert into article_image(id_article, id_image, largeur_image) values(8, 2, 250);
insert into article_image(id_article, id_image, largeur_image) values(10, 3, 100);
insert into article_image(id_article, id_image, largeur_image) values(8, 3, 150);
insert into article_image(id_article, id_image, largeur_image) values(2, 1, 250);
insert into article_image(id_article, id_image, largeur_image) values(7, 2, 250);
insert into article_image(id_article, id_image, largeur_image) values(7, 3, 100);
insert into article_image(id_article, id_image, largeur_image) values(7, 4, 250);
