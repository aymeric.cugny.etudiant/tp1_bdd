-- exercice 1
-- Q1
create table journaliste(id_journaliste serial primary key, nom_journaliste varchar(20), prenom_journaliste varchar(20),
		mel_journaliste varchar(100));

create table image(id_image serial primary key,
		legende_image varchar(20),
		url_image varchar(100));

create table rubrique(id_rubrique serial primary key,
		titre_rubrique varchar(20),
		intro_rubrique varchar(250),
		id_rubrique_sup int references rubrique on delete set null on update cascade,
		id_image int references image on delete set null on update cascade);

create table article(id_article serial primary key,
		titre_article varchar(20),
		texte_article text,
		chapo_article varchar(250),
		id_rubrique int references rubrique on delete restrict on update cascade);

create table breve(id_breve serial primary key,
		titre_breve varchar(20),
		texte_breve varchar(250),
		id_rubrique int references rubrique on delete restrict on update cascade,
		id_journaliste int references journaliste on delete set null on update cascade);

create table piece_jointe(id_piece_jointe serial primary key,
		nom_piece_jointe varchar(20),
		url_piece_jointe varchar(100));

create table article_image(id_article int references article on delete cascade on update cascade,
		id_image int references image on delete cascade on update cascade,
		largeur_image int,
		primary key(id_article, id_image));

create table article_piece_jointe(id_article int references article on delete cascade on update cascade,
		id_piece_jointe int references piece_jointe on delete cascade on update cascade,
		primary key(id_article, id_piece_jointe));

create table journaliste_article(id_journaliste int references journaliste on delete cascade on update cascade,
		id_article int references article on delete cascade on update cascade,
		primary key(id_journaliste, id_article));

create table statut(id_statut serial primary key, nom_statut varchar(20), descriptif_statut varchar(250));

create table journaliste_statut(id_journaliste int references journaliste on delete cascade on update cascade,
		id_statut int references statut on delete cascade on update cascade,
		id_rubrique int references rubrique on delete cascade on update cascade,
		primary key(id_journaliste, id_statut, id_rubrique));

create table etat(id_etat serial primary key, nom_etat varchar(20), descriptif_etat varchar(250));

create table histo_article(id_article int references article on delete cascade on update cascade,
		id_etat int references etat on delete cascade on update cascade,
		date_debut_etat_a date, date_fin_etat_a date,
		primary key(id_article, id_etat, date_debut_etat_a));

create table histo_breve(id_breve int references breve on delete cascade on update cascade,
		id_etat int references etat on delete cascade on update cascade,
		date_debut_etat_b date, date_fin_etat_b date,
		primary key(id_breve, id_etat, date_debut_etat_b));

